;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  (setq-default
   dotspacemacs-distribution 'spacemacs
   dotspacemacs-configuration-layer-path '("/home/matt/.config/spacemacs-layers/")
   dotspacemacs-configuration-layers
   '(rust
     bibtex
     (auto-completion :variables
                      auto-completion-return-key-behavior 'nil
                      auto-completion-tab-key-behavior 'complete
                      disabled-for 'eshell)
     colors
     csv
     docker
     (llm-client :variables
                 llm-client-enable-gptel t)
     emacs-lisp
     ess
     git
     helm
     html
     ipython-notebook
     javascript
     latex
     lua
     markdown
     openai
     (org :variables
          org-enable-reveal-js-support t
          org-hide-emphasis-markers t
          org-src-tab-acts-natively nil)
     pdf
     polymode
     ;;python
     (python :variables python-backend 'anaconda
             python-formatter 'yapf)
     (ranger :variables
             ranger-show-preview t)
     (shell :variables
            shell-default-shell 'eshell
            shell-default-height 35
            shell-default-position 'bottom
            shell-default-full-span nil
            :packages (not company))
     shell-scripts
     spacemacs-layouts
     spell-checking
     sql
     syntax-checking
     theming
     version-control
     yaml
     )

   dotspacemacs-additional-packages
   '(
     f
     fold-this
     sqlite3
     vimish-fold
                                        ;regexp
                                        ;key-chord
                                        ;bongo
                                        ;forecast
     ;;mu4e-alert
     ;;(jedi :location elpa)
                                        ;ssh
                                        ;helm-google
                                        ;ov
                                        ;smtpmail ;; necessary?
     )
   dotspacemacs-excluded-packages '()
   dotspacemacs-delete-orphan-packages t))

(defun dotspacemacs/init ()
  (setq-default
   dotspacemacs-elpa-https t
   dotspacemacs-elpa-timeout 5
   dotspacemacs-check-for-update t
   dotspacemacs-editing-style 'vim
   dotspacemacs-verbose-loading nil
   dotspacemacs-startup-banner 'random
   dotspacemacs-startup-lists nil
   dotspacemacs-startup-recent-list-size 5
   dotspacemacs-scratch-mode 'Fundamental
   dotspacemacs-themes '(
                         monokai
                         gruvbox-dark-medium
                         spacemacs-dark
                         noctilux
                         badwolf
                         spacemacs-dark
                         monokai
                         zenburn)
   dotspacemacs-colorize-cursor-according-to-state t
   ;; use different size font on laptop/desktop
   dotspacemacs-default-font (cond ((string-equal (system-name) "gram")
                                    '("Terminess Nerd Font Mono"
                                      :size 32
                                      :weight normal
                                      :width normal
                                      :powerline-scale 1.00))
                                   ((string-equal (system-name) "960-evo")
                                    '("Terminess Nerd Font Mono"
                                      :size 18
                                      :weight normal
                                      :width normal
                                      :powerline-scale 1.00))
                                   ((string-equal (system-name) "office")
                                    '("Terminess Nerd Font Mono"
                                      :size 22
                                      :weight normal
                                      :width normal
                                      :powerline-scale 1.00)))

   dotspacemacs-leader-key "SPC"
   dotspacemacs-emacs-leader-key "M-m"
   dotspacemacs-major-mode-leader-key ","
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   dotspacemacs-distinguish-gui-tab nil
   dotspacemacs-command-key ":"
   dotspacemacs-remap-Y-to-y$ t
   dotspacemacs-default-layout-name "Default"
   dotspacemacs-display-default-layout nil
   dotspacemacs-auto-resume-layouts nil
   dotspacemacs-auto-save-file-location 'cache
   dotspacemacs-max-rollback-slots 5
   dotspacemacs-use-ido nil
   dotspacemacs-helm-resize nil
   dotspacemacs-helm-no-header nil
   dotspacemacs-helm-position 'bottom
   dotspacemacs-enable-paste-micro-state nil
   dotspacemacs-which-key-delay 0.1
   dotspacemacs-which-key-position 'bottom
   dotspacemacs-loading-progress-bar t
   dotspacemacs-folding-method 'vimish
   dotspacemacs-fullscreen-at-startup nil
   dotspacemacs-fullscreen-use-non-native nil
   dotspacemacs-maximized-at-startup nil
   dotspacemacs-active-transparency 90
   dotspacemacs-inactive-transparency 90
   dotspacemacs-mode-line-unicode-symbols nil
   dotspacemacs-mode-line-theme '(spacemacs :separator slant :separator-scale 1.3)
   dotspacemacs-smooth-scrolling t
   dotspacemacs-line-numbers '(:visual t
                                       :disabled-for-modes dired-mode
                                       doc-view-mode
                                       pdf-view-mode
                                       :size-limit-kb 1000)
   dotspacemacs-smartparen-strict-mode nil
   dotspacemacs-highlight-delimiters 'all
   dotspacemacs-persistent-server nil
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   dotspacemacs-default-package-repository nil
   dotspacemacs-whitespace-cleanup nil
   evil-escape-key-sequence "jk"
   ))

(defun dotspacemacs/user-init ()

  ;; chatgpt stuff here
  (with-eval-after-load 'gptel
    (setq gptel-api-key
          (substring (f-read-text "/home/matt/git-repos/credentials/openai-key2.txt") 0 -1))

    (defun my-gptel-hook ()
      (local-set-key (kbd "C-c RET") 'spacemacs//gptel-send-wrapper))

    (add-hook 'gptel-mode-hook 'my-gptel-hook)

    (defun my-gptel-setup ()
      (toggle-truncate-lines 1)
      (setq word-wrap t)
      (auto-fill-mode -1))

    (add-hook 'gptel-mode-hook 'my-gptel-setup))

                                        ;  (defun haff/fold-rmd ()
                                        ;    "Fold current or next Rmd code block; unfold it if it's already folded"
                                        ;    (interactive)
                                        ;    (save-excursion
                                        ;      ;; search for end of code block in front of point
                                        ;      (search-forward "```\n" nil t)
                                        ;      ;; once there, search backward for beginning of code block
                                        ;      (search-backward "```{" nil t)
                                        ;      (setq beg (point))
                                        ;      (search-forward "```\n")
                                        ;      (setq end (point))
                                        ;      (vimish-fold beg end))
                                        ;    (search-forward "```\n")
                                        ;    (forward-line 1))
                                        ;
                                        ;  (defun haff/fold-rmd-all ()
                                        ;    "Fold all Rmd code blocks"
                                        ;    (interactive)
                                        ;    (vimish-fold-delete-all)
                                        ;    (save-excursion
                                        ;      (beginning-of-buffer)
                                        ;      (setq num-blocks (how-many "```{"))
                                        ;      (dotimes (number num-blocks)
                                        ;        (haff/fold-rmd))))
                                        ;
                                        ;  ;; not sure this is relevant
                                        ;  (defun haff/unfold-rmd-all ()
                                        ;    (interactive)
                                        ;    "Convenience function for removing all folds"
                                        ;    (vimish-fold-unfold-all))
                                        ;
                                        ;  (defun haff/fold-unfold-all ()
                                        ;    "Initiate folds in buffer"
                                        ;    (interactive)
                                        ;    (haff/fold-rmd-all)
                                        ;    (vimish-fold-unfold-all))

  (defun re-seq (regexp string n)
    "Get a list of all regexp matches in a string"
    (save-match-data
      (let ((pos 0)
            matches)
        (while (string-match regexp string pos)
          (add-to-list 'matches (+ n (string-match regexp string pos)) t)
          (setq pos (match-end 0)))
        matches)))

  (defun haff/fold-rmd-init ()
    (interactive)
    (save-excursion
      (vimish-fold-delete-all)
      (progn
        (setq begs-all (re-seq "```{" (buffer-string) 1))
        (setq ends-all (re-seq "```$" (buffer-string) 1))
        (setq begs begs-all)
        (setq ends ends-all)
        (add-to-list 'begs (point-max) t)
        (while ends
          (progn
            (ignore-errors (vimish-fold (nth 0 begs) (nth 0 ends)))
            (pop begs)
            (pop ends)
            ))
        )
      (vimish-fold-unfold-all)
      ))

  ;; TODO need to simplify this a bit to avoid reuse
  (defun haff/fold-rmd-all-blocks ()
    (interactive)
    (save-excursion
      (vimish-fold-delete-all)
      (progn
        (setq begs-all (re-seq "```{" (buffer-string) 1))
        (setq ends-all (re-seq "```$" (buffer-string) 1))
        (setq begs begs-all)
        (setq ends ends-all)
        (add-to-list 'begs (point-max) t)
        (while ends
          (progn
            (ignore-errors (vimish-fold (nth 0 begs) (nth 0 ends)))
            (pop begs)
            (pop ends)
            ))
        )
      ))

  (defun haff/unfold-all-rmd-blocks ()
    (interactive)
    (vimish-fold-unfold-all))

  ;; when in rmd files, initiate folding/unfolding of all code blocks
  (add-hook 'find-file-hook #'haff/fold-rmd-init)


  (setq helm-completion-style 'helm-fuzzy)
                                        ;(setq helm-completion-style 'emacs)
  (setq completion-styles '(flex))

                                        ;(setq org-src-fontify-natively t)
  ;; trying to get autocomplete to work properly
  ;;(add-hook 'python-mode-hook 'company-mode)
  ;;(add-hook 'python-mode-hook 'autocomplete-mode)

                                        ;(setq evil-want-keybinding nil)

  ;; added to fix error message about lsp: error use-package lsp-mode/:config
  ;; cannot open load file no such file or directory lsp-clients
                                        ;(setq configuration-layer-elpa-archives
                                        ;      '(("melpa"    . "melpa.org/packages/")
                                        ;        ("org"      . "orgmode.org/elpa/")
                                        ;        ("gnu"      . "elpa.gnu.org/packages/")))

  ;; TODO figure out how to reference variables (e.g., org-level-1) instead of
  ;; raw values
  (setq theming-modifications '(
                                ;; requires the theming layer
                                (monokai
                                        ;(font-latex-sectioning-2-face :foreground `(org-level-1))
                                 (font-latex-sectioning-2-face :foreground "#FD971F")
                                 (font-latex-sectioning-3-face :foreground "#A6E22E")
                                 (font-latex-sectioning-4-face :foreground "#66D9EF")
                                 (font-latex-sectioning-5-face :foreground "#C678DD")
                                 (markdown-header-face-1 :foreground "#FD971F")
                                 (markdown-header-face-2 :foreground "#A6E22E")
                                 (markdown-header-face-3 :foreground "#66D9EF")
                                 (markdown-header-face-4 :foreground "#C678DD")
                                 (markdown-header-face-5 :foreground "#FD971F")
                                 )
                                )
        )
  )

(defun dotspacemacs/user-config ()

  (defun read-file-contents (file-path)
    "Read the contents of FILE-PATH and return it as a string."
    (with-temp-buffer
      (insert-file-contents file-path)
      (buffer-string)))

  (setq openai-key "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")
  ;;(setq openai-key (read-file-contents "/home/matt/git-repos/credentials/openai-key.txt"))
  (setq openai-user "geographer123")

  ;; set python version
  (setq python-shell-interpreter "/usr/bin/python")

  ;; TODO check if fold exists, if so, refold
  (defun haff/fold-rmd ()
    "Fold Rmd code block"
    (interactive)
    (save-excursion
      (search-backward "```{")
      (setq beg (point))
      (evil-forward-char 1)
      (search-forward "```")
      (setq end (point))
      (vimish-fold beg end))
    (search-forward "```")
    (forward-line 2))

  ;; TODO create function which folds all rmd blocks

  ;; keep undo-tree from putting files in the current working directory
  (setq undo-tree-auto-save-history nil)

  ;; start R in the folder of the buffer file where ess process was initiated
  (setq ess-ask-for-ess-directory t)

  ;; enable evil-escape mode by default
  (evil-escape-mode 1)

  ;; TODO clean up variable declarations
  ;; TODO map this function to a key
  (defun haff/blogdown-build-site ()
    "Render blogdown site with custom command if 'build-site.sh'
     file exists in project root. Otherwise, use blogdown::build_site"
    (interactive)
    (setq git-dir-exists nil)
    (setq git-location ".git")
    (setq current-dir default-directory)
    (while (not git-dir-exists)
      (if (file-exists-p ".git")
          (setq git-dir-exists t)
        (progn
          (setq git-location (concat "../" git-location))
          (cd "../"))))
    (shell-command "bash serve-site.sh")
    (cd current-dir))

  ;; TODO clean this up a bunch...hide process in another buffer? TODO account
  ;; for sites (geohaff.com) using an older version of hugo/custom test-site.sh
  ;; command
  (defun haff/blogdown-test-site ()
    "Test site with hugo serve"
    (interactive)
    (shell-command "hugo serve"))

  ;; fixes issue of helm pcomplete opening separate window
  (setq helm-show-completion-display-function #'helm-show-completion-default-display-function)

  ;; TODO remove whitespace (i.e. spaces) between line breaks. otherwise this
  ;; doesn't work as intended
  (defun haff/remove-single-line-breaks ()
    "Remove single line breaks but keep double line breaks. This
     is useful when typing responses in Emacs that you know will
     have to be submitted in a web form, for instance. You need
     to remove the 'soft' line breaks enforced by wrapping, but
     you want to preserve paragraph breaks.

     This was helpful:
     https://stackoverflow.com/questions/5194294/how-to-remove-all-newlines-from-selected-region-in-emacs"

    (interactive)
    (save-restriction
      (narrow-to-region (point) (mark))
      (goto-char (point-min))
      (while (search-forward "\n\n" nil t)
        (replace-match "XXn"))

      (goto-char (point-min))
      (while (search-forward "\n" nil t)
        (replace-match " "))

      (goto-char (point-min))
      (while (search-forward "XXn" nil t)
        (replace-match "\n\n"))))

  ;; enable rainbow mode by default which is helpful for things like xaringan
  ;; style files
  (add-hook 'prog-mode-hook 'rainbow-mode)

  (defun python-run-line ()
    "This function executes the line at which the point is
    located and then moves to the next line. Solution found here:
    https://stackoverflow.com/a/30774439"
    (interactive)
    (save-excursion
      (setq the_script_buffer (format (buffer-name)))
      (end-of-line)
      (kill-region (point) (progn (back-to-indentation) (point)))
      ;;(setq the_py_buffer (format "*Python[%s]*" (buffer-file-name)))
      (setq the_py_buffer "*Python*")
      (switch-to-buffer-other-window  the_py_buffer)
      (goto-char (buffer-end 1))
      (yank)
      (comint-send-input)
      (switch-to-buffer-other-window the_script_buffer)
      (yank))
    (forward-line))

                                        ; assign function to keybinding
  (add-hook 'python-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c C-x") 'python-run-line)))

  (defun haff/open-html-firefox ()
    "When working with Rmarkdown or .org files that have been
    rendered into .html, I frequently find myself entering
    commands like ':! firefox my-rmarkdown.html' to open the
    current .Rmd file (often newly rendered) in a browser. If the
    .html file is already open in a browser, you can simply
    refresh it, but I often use emacs as a navigation tool, then
    open files from the evil shell (correct name?).

    This function gets the current buffer name, strips the 'Rmd'
    suffix, appends 'html' to it, and opens the file in firefox."
    (interactive)
    ;; create the new string of the file to open
    (setq file-string (concat (file-name-sans-extension (buffer-file-name)) ".html"))
    ;; execute command
    (shell-command (concat "firefox " file-string)))

  ;; assign this function to a keybinding in markdown mode
  (add-hook 'markdown-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c f") 'haff/open-html-firefox)))

  ;; assign this function to a keybinding in org mode
  (add-hook 'org-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c f") 'haff/open-html-firefox)))

  (defun haff/rmd-render ()
    "Spawning an R process is a pain to simply render an RMarkdown
     file from within Emacs. Here I do it as a function. Must be in
     the buffer you want to render."
    (interactive)
    ;; build the R render command
    (setq rcmd (concat "rmarkdown::render('" buffer-file-name "')"))
    ;; pipe the r command to r from the terminal
    (setq command (concat "echo \"" rcmd "\" | R --vanilla"))
    ;; i have no idea why compile is used here but it works
    (compile command))

  ;; web mode keybindings
  (add-hook 'web-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c f") 'haff/open-html-firefox)))

  ;; markdown mode keybindings
  (add-hook 'markdown-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c f") 'haff/open-html-firefox)
              (local-set-key (kbd "C-c s") 'haff/add-src-block-md)
              (local-set-key (kbd "C-c r") 'haff/rmd-render)))

  ;; yaml mode keybindings
  (add-hook 'yaml-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c f") 'haff/open-html-firefox)
              (local-set-key (kbd "C-c r") 'haff/rmd-render)))

  ;; ess mode keybindings
  (add-hook 'ess-r-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c f") 'haff/open-html-firefox)
              (local-set-key (kbd "C-c r") 'haff/rmd-render)))

  (setq countries '("Afghanistan" "Pakistan" "Ukraine" "Czech Republic"))

  (add-to-list 'auto-mode-alist '("\\.md" . poly-markdown-mode))

                                        ; used to kill the compiler buffer if everything goes ok
  (add-hook 'compilation-finish-functions (lambda (buf strg) (kill-buffer buf)))

                                        ; start text modes (which includes markdown) with autofill mode enabled
  (add-hook 'text-mode-hook 'turn-on-auto-fill)
  (add-hook 'text-mode-hook 'spacemacs/toggle-visual-line-numbers)

                                        ; start prog-mode with line numbers
  (add-hook 'prog-mode-hook 'spacemacs/toggle-visual-line-numbers)

                                        ; change mode of .Rmd files to markdown
                                        ;(add-to-list 'auto-mode-alist '("\\.Rmd\\'" . markdown-mode))

                                        ; disable linum-mode in pdf-tools; it's unusable otherwise
  (add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1)))

                                        ; modify default tramp method
  (setq tramp-default-method "ssh")

  ;; keep emacs from hanging during search (particularly in useful-commands.org
  (setq-default search-invisible t)
  (setq-default isearch-invisible t)

                                        ; change org bullets
  (setq org-superstar-bullet-list '("■" "◆" "▲" "▶"))

                                        ; use US English dictionary
  (setq ispell-local-dictionary "en_US")

                                        ; use transparency
  (spacemacs/toggle-transparency)

                                        ; disable smart-parens everywhere
  (remove-hook 'prog-mode-hook #'smartparens-mode)

                                        ; set a liberal scroll margin
  (setq scroll-margin 20)

  ;; disable the liberal scroll margin in shells
  (add-hook 'inferior-ess-r-mode-hook
            (lambda ()
              (setq-local scroll-margin 1)))

  ;; disable the liberal scroll margin in shells
  (add-hook 'inferior-python-mode-hook
            (lambda ()
              (setq-local scroll-margin 1)))

  ;; copy aliases from private dir (version controlled) to location that eshell
  ;; expects
  (with-eval-after-load
      (copy-file "~/.emacs.d/private/alias" "~/.emacs.d/.cache/eshell/alias" t))

                                        ;(with-eval-after-load "company"
                                        ;  '(add-to-list 'company-backends 'company-anaconda))

  ;; disable the liberal scroll margin in shells
  (with-eval-after-load 'eshell
    (add-hook 'eshell-mode-hook
              (lambda ()
                (setq-local scroll-margin 1))))

  ;; Define custom functions
  (defun haff/duplicate-slide ()
    "Duplicate xarinagan slide below the next three dashes"
    (interactive)
    (save-excursion
      ;; get current text to search forward to it after pasting new slide content
      (setq slide-lines-before (count-lines (point-min) (point-max)))
      (setq cur-text (buffer-substring (point) (line-end-position)))
      (if
          (search-forward "---"
                          nil
                          t)
          ;; then
          (progn
            (end-of-line)
            (insert "\n"))
        ;; else
        (progn
          (end-of-buffer)
          (insert "\n")
          (forward-line)
          (insert "---")
          (forward-line)
          (end-of-line)))
      (setq end (point))
      (search-backward "---")
      (search-backward "---")
      (forward-line)
      (setq beg (point))
      (evil-yank beg end)
      (search-forward "---")
      (forward-line)
      (evil-paste-after 1))
    (setq slide-lines-after (count-lines (point-min) (point-max)))
    (forward-line (- slide-lines-after slide-lines-before 1))
    (search-backward cur-text))

  (defun haff/delete-slide ()
    "Delete xarinagan slide between the three dashes below (if any) and the three dashes above"
    (interactive)
    (save-excursion
      (if
          (search-forward "---" nil t)
          ;; then
          (progn
            (end-of-line)
            (setq slide-point-end (point)))
        ;; else
        (progn
          (end-of-line)
          (setq slide-point-end (point))))
      (beginning-of-line)
      (search-backward "---")
      (forward-line)
      (beginning-of-line)
      (setq slide-point-beg (point))
      (delete-region slide-point-beg slide-point-end)
      (delete-blank-lines)))

  (defun add-src-block (MODE)
    "Make adding source code blocks easier in various files"
    (progn
      (setq loc-var (point))
      (if (string= MODE "org")
          (progn
            (setq begin-text "#+BEGIN_SRC")
            (setq end-text "\n#+END_SRC\n")
            (setq full-text "#+BEGIN_SRC\n\n#+END_SRC\n"))
        (progn
          (setq begin-text "```{")
          (setq end-text "\n\n```\n")
          (setq full-text "```{r echo = TRUE, eval = TRUE, message = FALSE, warning = FALSE}\n\n```\n"))
        )
      (if (search-backward begin-text nil t nil)
          (progn
            (goto-char loc-var)
            (search-backward begin-text nil t nil)
            (evil-yank (line-beginning-position) (line-end-position))
            (goto-char loc-var)
            (evil-paste-after 1)
            (forward-line)
            (insert end-text)
            (forward-line -2)
            (evil-insert-state))
        (progn
          (insert full-text)
          (forward-line -2)
          (evil-insert-state)))))

  (defun haff/add-src-block-org ()
    "Add source block in org files"
    (interactive)
    (add-src-block 'org))

  (defun haff/add-src-block-md ()
    "Add source block in markdown (or RMarkdown) files"
    (interactive)
    (add-src-block 'md))

  (defun yas-img-block ()
    "Insert image block used in xaringan presentations"
    (search-backward "', dpi = NA")
    (evil-insert-state)
    )

  (setq chunk-dir "~/.emacs.d/private/chunks/")

  ;; TODO add check for whether or not thing to paste in from google maps is a
  ;; list of two numbers
  ;;
  (defun haff/xar-add-web-map (COLUMNS CHUNK-DIR)
    "Insert web map used in xaringan presentations. It inserts a map from the 'chunks' dir"
    (if (= COLUMNS 1)
        (progn
          (setq start-text "<!-- one column web map (start) -->")
          (setq end-text "<!-- one column web map (end) -->")
          (setq choices '("OSM Standard" "Esri.WorldImagery"))
          (setq basemap (ido-completing-read "Basemap: " choices))
          (cond
           ((string= basemap "OSM Standard")
            (setq insert-filename "web-map.Rmd"))
           ((string= basemap "Esri.WorldImagery")
            (setq insert-filename "web-map-satellite.Rmd"))))
      (progn
        (setq start-text "<!-- two column web map (start) -->")
        (setq end-text "<!-- two column web map (end) -->")
        (setq insert-filename "web-map-two-column.Rmd")))
    (setq chunk-dir CHUNK-DIR)
    (setq loc-var (point))
    ;; if previous web map exists
    (if (search-backward start-text nil t nil)
        (progn ; then
          (if
              (y-or-n-p "Use previous web map?")
              (progn
                (search-backward start-text nil t nil)
                (setq copy-start-point (point))
                (search-forward end-text nil t nil)
                (move-end-of-line nil)
                (setq copy-end-point (point))
                (copy-region-as-kill copy-start-point copy-end-point)
                (goto-char loc-var)
                (yank)
                (search-backward "loc <- c(")
                (search-forward "(")
                (forward-char 1))
            (progn
              (goto-char loc-var)
              (insert-file-contents (concat chunk-dir insert-filename)))))
      (progn ; else
        (goto-char loc-var)
        (insert-file-contents (concat chunk-dir insert-filename))))

    (search-forward "loc <- c(")
    (move-end-of-line nil)
    (forward-char -1)
    (if (y-or-n-p "Have coordinates from Google Maps?")
        (progn
          (evil-paste-before 1))
      (evil-insert-state)))

  (defun haff/xar-add-img-from-link ()
    "Add an image link to a xaringan slide"
    (interactive)
    (insert-file-contents (concat chunk-dir "img-from-source.Rmd"))
    (if
        (y-or-n-p "Image height '100%'?")
        ;; if true
        (setq HEIGHT "100%")
      ;; if false
      (progn
        (setq HEIGHT
              (number-to-string
               (read-number "Image height in pixels: " 400)))))
    (search-forward "height='")
    (insert HEIGHT)
    (search-backward "src='")
    (if
        (y-or-n-p "Have link in clipboard?")
        ;; if true
        (progn
          (forward-char 4)
          (evil-paste-after 1)
          (forward-line 2))
      ;; if false
      (forward-char 4))
    )

  (defun haff/xar-add-web-map-one-column ()
    "Add a xaringan slide with a one column web map"
    (interactive)
    (haff/xar-add-web-map 1 chunk-dir))

  (defun haff/xar-add-iframe-from-link ()
    "Add an iframe to a xaringan slide"
    (interactive)
    (setq HEIGHT (read-number "Iframe height in pixels: " 625))
    (insert-file-contents (concat chunk-dir "iframe.Rmd"))
    (search-forward "height='")
    (insert (number-to-string HEIGHT))
    (search-backward "src='")
    (if
        (y-or-n-p "Have link in clipboard?")
        ;; if true
        (progn
          (forward-char 4)
          (evil-paste-after 1)))
    (forward-line 2))

  (defun haff/xar-add-web-map-two-column ()
    "Add a xaringan slide with a two column web map"
    (interactive)
    (haff/xar-add-web-map 2 chunk-dir))

  (defun haff/xar-add-country-map ()
    "Add a tmap map of a particular country"
    (interactive)
    ;;(setq chunk-dir CHUNK-DIR)
    (insert-file-contents (concat chunk-dir "country-map-one-column.Rmd"))
    ;; TODO fix this up
    (search-backward "country_name")
    (search-backward "country_name")
    (move-end-of-line nil)
    (forward-char -1)
    (setq country (ido-completing-read "Country: " countries))
    (insert country))

  (defun haff/xar-add-iframe ()
    "Add iframe and paste a link from the clipboard"
    (interactive)
    (insert-file-contents (concat chunk-dir "iframe.html"))
    (search-forward "src=")
    (forward-char)
    (if (y-or-n-p "Have link ready to paste?")
        (progn
          (evil-paste-before 1))
      (evil-insert-state)))

  (defun haff/xar-two-column-slide ()
    "Add a xaringan slide with two columns"
    (interactive)
    (progn
      (setq choices '("50/50" "40/60" "70/30" "30/70"))
      (setq split-type (ido-completing-read "Split type: " choices))
      (cond
       ((string= split-type "50/50")
        (insert-file-contents (concat chunk-dir "two-column-50-50.Rmd")))
       ((string= split-type "40/60")
        (insert-file-contents (concat chunk-dir "two-column-40-60.Rmd")))
       ((string= split-type "70/30")
        (insert-file-cotents (concat chunk-dir "two-column-70-30.Rmd")))
       ((string= split-type "30/70")
        (insert-file-contents (concat chunk-dir "two-column-30-70.Rmd")))
       (evil-insert-state))))

  (defun haff/xar-countdown ()
    "Add a countdown timer into a xaringan presentation"
    (interactive)
    (progn
      (setq MINUTES
            (number-to-string
             (read-number "Countdown timer in minutes: " 2))))
    (insert-file-contents (concat chunk-dir "countdown.Rmd"))
    (search-forward "minutes = ")
                                        ;(forward-char)
    (insert MINUTES)
    (forward-line 2))

  ;; TODO create function for making simple map of a country with some context

  (defun haff/zoom-frm-n-times (N DIRECTION)
    "Zoom in/out n times"
    (setq i 0)
    (while (< i N)
      (if (string= DIRECTION "in")
          (progn
            (zoom-frm-in)
            (setq i (+ i 1)))
        (progn
          (zoom-frm-out)
          (setq i (+ i 1))))))

  (defun haff/zoom-frm-n-times-in ()
    "Zoom in n times (default of 4)"
    (interactive)
    (setq n (read-number "Number of times to zoom in: " 4))
    (haff/zoom-frm-n-times n "in"))

  (defun haff/zoom-frm-n-times-out ()
    "Zoom in n times (default of 4)"
    (interactive)
    (setq n (read-number "Number of times to zoom out: " 4))
    (haff/zoom-frm-n-times n "out"))

  ;; some keybindings
  ;; NOTE moved this to the variable configurations section and it seems to be working?
  ;;(setq-default evil-escape-key-sequence "jk") ; set escape keybinding to "jk"

                                        ; set some variables
  (setq
   spacemacs-useless-buffers-regexp '("\\*helm\.+\\*") ; make only helm buffers useless
                                        ;powerline-default-separator 'utf-8
   vc-follow-symlinks nil
   org-reveal-root "")
                                        ;org-reveal-root "/home/matt/git-repos/reveal.js")

                                        ; some other variables
  (set-default 'truncate-lines t)
  (set-fill-column 70)
  (setq bibtex-completion-additional-search-fields '(tags))
                                        ;(setq ess-ask-for-ess-directory nil)
                                        ;(add-hook 'ess-mode-hook 'linum-mode)
  (add-to-list 'auto-mode-alist '("README" . org-mode))

                                        ; org settings
  (with-eval-after-load 'org ; must be evaluated after load to prevent version conflicts
    (add-hook 'org-mode-hook 'auto-fill-mode)
    (add-hook 'org-mode-hook 'abbrev-mode)
    (add-hook 'org-mode-hook 'flyspell-mode)
    (add-hook 'org-mode-hook
              (lambda ()
                (local-set-key (kbd "C-c s") 'haff/add-src-block-org)))
                                        ;(add-to-list 'org-export-backends 'org) ; doesn't work
    (setq org-ref-default-bibliography '("~/org/work/references/references.bib")
          org-ref-pdf-directory "~/org/work/references/pdfs"
          org-ref-bibliography-notes "~/org/work/references/notes.org")
                                        ; enables bibliography at end of document
    (setq org-latex-pdf-process '("latexmk -xelatex -quiet -shell-escape -f %f"))
    ;; this below may be needed instead
                                        ;    (setq org-latex-pdf-process
                                        ;          '("pdflatex -interaction nonstopmode -output-directory %o %f"
                                        ;            "bibtex %b"
                                        ;            "pdflatex -interaction nonstopmode -output-directory %o %f"
    (add-hook 'org-mode-hook (lambda () (setq fill-column 70)))
    (setq org-agenda-files '("~/MEGA/megasync/agenda"))
    (setq org-startup-indented t)
    ;; this breaks org-mode
    (setq org-bullets-bullet-list '("■" "◆" "▲" "▶"))
    (setcar (nthcdr 2 org-emphasis-regexp-components) " \t\r\n,\"")
    (setcar (nthcdr 4 org-emphasis-regexp-components) 100)
    (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)
    )

  (with-eval-after-load 'helm
    (define-key helm-map (kbd "C-d") 'helm-next-page)
    (define-key helm-map (kbd "C-u") 'helm-previous-page))

  (with-eval-after-load 'flyspell
    (define-key flyspell-mode-map (kbd "C-c C-SPC") 'flyspell-auto-correct-word))

                                        ; this needs to be separate from other setq's?
                                        ;  (setq
                                        ;   calendar-latitude 44.7967
                                        ;   calendar-location-name "Eau Claire, WI"
                                        ;   calendar-longitude -91.5000
                                        ;   forecast-units "si")

  ;; define some custom layouts
  (spacemacs|define-custom-layout "home-desktop"
    :binding "h d"
    :body
    (spacemacs/find-dotfile)
    (spacemacs/layout-triple-columns)
    (winum-select-window-2)
    (find-file "~/Sync/agenda/todo-list.org")
    (spacemacs/default-pop-shell)
    (winum-select-window-4)
    (forecast)
    (persp-add-buffer (buffer-name))
    (shrink-window-horizontally 20))

  (spacemacs|define-custom-layout "home-laptop"
    :binding "h l"
    :body
    (spacemacs/find-dotfile)
    (find-file "~/Sync/agenda/todo-list.org")
    (split-window-right-and-focus)
    (spacemacs/default-pop-shell)
    (winum-select-window-2)
    (forecast))

  (spacemacs|define-custom-layout "shp2nosql"
    :binding "s"
    :body
    (find-file "~/git-repos/shp2nosql/README")
    (split-window-right-and-focus)
    (find-file "~/git-repos/shp2nosql/bin/shp2es")
    (find-file "shp2mongo")
    (winum-select-window-1)
    (spacemacs/default-pop-shell))

  (spacemacs|define-custom-layout "non-english-tweets"
    :binding "n"
    :body
    (R)
    (find-file "~/git-repos/non-english-tweets/README.org")
    (split-window-right-and-focus)
    (find-file "~/git-repos/non-english-tweets/statistical-analysis/data-preparation.R")
    (find-file "regression-analysis.R")
    (winum-select-window-1)
    (spacemacs/default-pop-shell))

  (defun haff/count-words-in-doc-section ()
    "This function counts the number of words in the section of a
  document. It may be useful when a manuscript's word count is
  limiting and you are working in an org-file with much more than
  just the document's text (e.g. you have notes, an outline, etc.).
  It looks for the tags 'begin_region_word_count' and
  'end_region_word_count' and prints the number of words two lines
  after the tag region_word_count_print."
    (interactive)
    (save-excursion
      (beginning-of-buffer)
      (let* ((section-beginning
              (search-forward "start_region_word_count"))
             (section-end
              (search-forward "end_region_word_count"))
             (word-count
              (count-region section-beginning section-end)))
        (print word-count))))
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(evil-want-Y-yank-to-eol t)
   '(package-selected-packages
     '(lsp-mode counsel-gtags counsel swiper cargo zenburn-theme yasnippet-snippets yapfify yaml-mode xterm-color ws-butler writeroom-mode winum which-key web-mode web-beautify vterm volatile-highlights vi-tilde-fringe uuidgen use-package undo-tree treemacs-projectile treemacs-persp treemacs-magit treemacs-icons-dired treemacs-evil toc-org terminal-here tagedit symon symbol-overlay string-inflection sql-indent sphinx-doc spaceline-all-the-icons smeargle slim-mode shell-pop scss-mode sass-mode restart-emacs ranger rainbow-mode rainbow-identifiers rainbow-delimiters pytest pyenv-mode py-isort pug-mode prettier-js popwin poly-R poetry pippel pipenv pip-requirements pcre2el password-generator paradox overseer orgit org-superstar org-rich-yank org-ref org-re-reveal org-projectile org-present org-pomodoro org-mime org-download org-cliplink org-brain open-junk-file npm-mode nodejs-repl noctilux-theme nameless multi-term move-text monokai-theme mmm-mode markdown-toc magit-svn magit-gitflow macrostep lorem-ipsum livid-mode live-py-mode link-hint json-navigator js2-refactor js-doc insert-shebang indent-guide importmagic impatient-mode hybrid-mode hungry-delete hl-todo highlight-parentheses highlight-numbers highlight-indentation helm-xref helm-themes helm-swoop helm-pydoc helm-purpose helm-projectile helm-org-rifle helm-org helm-mode-manager helm-make helm-ls-git helm-gitignore helm-git-grep helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag gruvbox-theme google-translate golden-ratio gnuplot gitignore-templates gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe+ gh-md fuzzy forge font-lock+ flyspell-correct-helm flycheck-pos-tip flycheck-package flycheck-elsa flycheck-bashate flx-ido fish-mode fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-textobj-line evil-surround evil-org evil-numbers evil-nerd-commenter evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-escape evil-ediff evil-easymotion evil-collection evil-cleverparens evil-args evil-anzu eval-sexp-fu ess-R-data-view eshell-z eshell-prompt-extras esh-help emr emmet-mode elisp-slime-nav editorconfig dumb-jump dotenv-mode dockerfile-mode docker dired-quick-sort diminish devdocs define-word cython-mode csv-mode company-web company-shell company-reftex company-math company-lua company-auctex company-anaconda column-enforce-mode color-identifiers-mode clean-aindent-mode centered-cursor-mode browse-at-remote blacken badwolf-theme auto-yasnippet auto-highlight-symbol auto-dictionary auto-compile auctex-latexmk aggressive-indent ace-link ace-jump-helm-line ac-ispell)))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(font-latex-sectioning-2-face ((t (:foreground "#FD971F"))))
   '(font-latex-sectioning-3-face ((t (:foreground "#A6E22E"))))
   '(font-latex-sectioning-4-face ((t (:foreground "#66D9EF"))))
   '(font-latex-sectioning-5-face ((t (:foreground "#C678DD"))))
   '(markdown-header-face-1 ((t (:foreground "#FD971F"))))
   '(markdown-header-face-2 ((t (:foreground "#A6E22E"))))
   '(markdown-header-face-3 ((t (:foreground "#66D9EF"))))
   '(markdown-header-face-4 ((t (:foreground "#C678DD"))))
   '(markdown-header-face-5 ((t (:foreground "#FD971F")))))
  )
