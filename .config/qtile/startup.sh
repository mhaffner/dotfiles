#!/bin/bash

if [[ "$(hostname)" = "960-evo" ]]; then
    # monitor layout
    xrandr --output DP-1 --auto --primary --output DVI-D-1 --auto --above DP-1

    # desktop background
    feh --bg-center ~/org/personal/pictures/backgrounds/2560x1080/space/space-2.jpg

    # restart qtile to fix the bar (otherwise does not appear on screen); also,
    # terminals only appear on workspace sys unless qtile is restarted
    echo "restart" | qshell
elif
    [[ "$(hostname)" = "dell-laptop" ]]; then
    xmodmap ~/.XmodMap

    # desktop background
    feh --bg-center ~/Sync/pictures/backgrounds/1920x1080/space/space-12.jpg
    # feh --bg-center ~/Sync/pictures/backgrounds/1920x1080/space/space-15.jpg
elif
    [[ "$(hostname)" = "office" ]]; then

    # four monitor layout
	  xrandr --output DP-4 --auto --primary --output DP-6 --auto --left-of DP-4 --output DP-6 --auto --above DP-4 --output DP-0 --auto --right-of DP-4 --rotate left

    # desktop background
    # feh --bg-center ~/org/personal/pictures/backgrounds/2560x1440/mountains-12.jpg
    feh --bg-center ~/org/personal/pictures/backgrounds/2560x1440/city-19.jpg
elif
    [[ "$(hostname)" = "manjaro" ]]; then
    xmodmap ~/.XmodMap

    feh --bg-center ~/Sync/pictures/backgrounds/1920x1080/space/space-12.jpg
elif
    [[ "$(hostname)" = "gram" ]]; then
    xmodmap ~/.XmodMap.gram
    feh --bg-center ~/org/personal/pictures/backgrounds/2560x1600/city-night-01.jpg
fi

# start compositor for transparency, -b flag for background
picom -b
