# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.config import EzKey
from libqtile.lazy import lazy
from libqtile import hook
from libqtile import layout, bar, widget, extension

import socket
import subprocess
import os

try:
    from typing import List  # noqa: F401
except ImportError:
    pass

mod = "mod4"

# on startup
@hook.subscribe.startup_once
def autostart():
    subprocess.call(["/home/matt/.config/qtile/startup.sh"])
    # need to add xmodmap ~/.XmodMap to startup script

keys = [
    # keybindings for bsp layout
    EzKey("M-h", lazy.layout.left()),
    EzKey("M-l", lazy.layout.right()),
    EzKey("M-<Left>", lazy.layout.left()),
    EzKey("M-<Right>", lazy.layout.right()),

    EzKey("M-S-h", lazy.layout.shuffle_left()),
    EzKey("M-S-l", lazy.layout.shuffle_right()),
    EzKey("M-S-<Left>", lazy.layout.shuffle_left()),
    EzKey("M-S-<Right>", lazy.layout.shuffle_right()),

    # resize windows
    EzKey("M-C-h", lazy.layout.grow_left()),
    EzKey("M-C-l", lazy.layout.grow_right()),
    Key([mod, "control"], "h", lazy.layout.grow_left(-30)),
    Key([mod, "control"], "l", lazy.layout.grow_right(30)),
    Key([mod, "control"], "j", lazy.layout.grow_up(-30)),
    Key([mod, "control"], "k", lazy.layout.grow_down(30)),

    # Switch between windows in current stack pane
    EzKey("M-j", lazy.layout.down()),
    EzKey("M-k", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Volume (for 60% keyboard)
    Key([mod], "equal", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([mod], "minus", lazy.spawn("amixer -c 0 -q set Master 2dB-")),

    # Volume (media keys)
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -q sset Master toggle")),

    # Screen brightness (for laptop)
    Key([], "XF86MonBrightnessUp", lazy.spawn('xbacklight -inc 10')),
    Key([], "XF86MonBrightnessDown", lazy.spawn('xbacklight -dec 10')),

    # TODO create function for putting rofi on proper screen; the issue here is
    # that rofi appears on whichever screen has the mouse and puts the next
    # window in that group; need to come up with a way to move the mouse to
    # group when switching

    # rofi
    EzKey("M-r", lazy.spawn("rofi -show combi -theme lb -m -1")),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),

    # switch to a screen explicitly
    EzKey("M-1", lazy.to_screen(0)),
    EzKey("M-2", lazy.to_screen(1)),
    EzKey("M-3", lazy.to_screen(2)),
    EzKey("M-4", lazy.to_screen(3)),

    # terminal
    EzKey("M-<Return>", lazy.spawn("terminator")),

    # dmenu
#    EzKey("M-C-d", lazy.run_extension(extension.DmenuRun(
#        dmenu_prompt=">",
#        dmenu_font="Andika-10",
#        background="#15181a",
#        foreground="#00ff00",
#        selected_background="#079822",
#        selected_foreground="#fff",
#        dmenu_height=24))),

    # Toggle between different layouts as defined below
    EzKey("M-S-<Tab>", lazy.next_layout()),

    # move to the group (i.e. workspace) on the right according to the bar
    #EzKey("M-l", lazy.screen.next_group()),

    # move to the group (i.e. workspace) on the left according to the bar
    #EzKey("M-h", lazy.screen.prev_group()),

    # switch to other screen
    EzKey("M-<Tab>", lazy.next_screen()),

    EzKey("M-S-q", lazy.window.kill()),
    EzKey("M-S-r", lazy.restart()),
    EzKey("M-S-e", lazy.shutdown()),
]

groups = [
    Group(name = "a", persist = True, init = True, label = "a: web"),
    Group(name = "s", persist = True, init = True, label = "s: sys", spawn = "terminator -e 'gtop'"),
    Group(name = "d", persist = True, init = True, label = "d: emacs", spawn = "emacs"),
    Group(name = "f", persist = True, init = True, label = "f: music"),
    Group(name = "z", persist = True, init = True, label = "z: pdf"),
    Group(name = "x", persist = True, init = True, label = "x: misc"),
    Group(name = "c", persist = True, init = True, label = "c: chrome"),
    Group(name = "v", persist = True, init = True, label = "v: discord")
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

layouts = [
    layout.Bsp(
        border_normal='#333333',
        border_focus='#00e891',
        border_width=1,
        margin=[10,10,10,10],
		margin_on_single=[10,10,10,10],
		grow_amount=20,
		lower_right=True
    ),
    layout.Max(),
    layout.Stack(num_stacks=2)
]

if socket.gethostname() == 'gram':
    fontsize = 24
else:
    fontsize = 18

widget_defaults = dict(
    font='Terminess Nerd Font Mono',
    fontsize=fontsize,
    padding=1,
)
extension_defaults = widget_defaults.copy()

ur = str(r'◥')
ul = str(r'◤')
lr = str(r'◢')
ll = str(r'◣')

colors = [["#AE81FF"],
          ["#FD971F"],
          ["#000000"]]

sep_1 = widget.Sep(
    linewidth = 0,
    padding = 50,
    foreground = colors[1],
    background = colors[0]
)

sep_left = widget.TextBox(
    text = "[",
    padding = 1,
)

sep_right = widget.TextBox(
    text = "]",
    padding = 1,
    foreground = "#ffffff"
)


screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentScreen(padding=10,
                                     active_text = "⬤",
                                     inactive_text = "⬤",
                                     active_color = colors[1],
                                     inactive_color = colors[2]),
                # to indicate screen number
                widget.TextBox(
                    text = " 1 ",
                    foreground = colors[0]),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.GroupBox(hide_unused = True),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.WindowName(foreground = colors[1]),
                widget.Battery(foreground = colors[1],
                               battery = "CMB0"),
                widget.TextBox(
                    text = "Volume: ",
                    padding = 1,
                ),
                widget.Volume(foreground = colors[1]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.Clock(
                    format='%m-%d-%Y %a',
                    foreground = colors[0]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.TextBox(
                    text = "",
                    padding = 1,
                ),
                widget.Clock(
                    format='%I:%M %p',
                    foreground = colors[1]),
            ],
            24,
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentScreen(padding=10,
                                     active_text = "⬤",
                                     inactive_text = "⬤",
                                     active_color = colors[1],
                                     inactive_color = colors[0]),
                # to indicate screen number
                widget.TextBox(
                    text = " 2 ",
                    foreground = colors[0]),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.GroupBox(hide_unused = True),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.WindowName(foreground = colors[1]),
                widget.TextBox(
                    text = "Volume: ",
                    padding = 1,
                ),
                widget.Volume(foreground = colors[1]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.Clock(
                    format='%m-%d-%Y %a',
                    foreground = colors[0]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.TextBox(
                    text = "",
                    padding = 1,
                ),
                widget.Clock(
                    format='%I:%M %p',
                    foreground = colors[1]),
            ],
            24,
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentScreen(padding=10,
                                     active_text = "⬤",
                                     inactive_text = "⬤",
                                     active_color = colors[1],
                                     inactive_color = colors[0]),
                # to indicate screen number
                widget.TextBox(
                    text = " 3 ",
                    foreground = colors[0]),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.GroupBox(hide_unused = True),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.WindowName(foreground = colors[1]),
                widget.TextBox(
                    text = "Volume: ",
                    padding = 1,
                ),
                widget.Volume(foreground = colors[1]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.Clock(
                    format='%m-%d-%Y %a',
                    foreground = colors[0]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.TextBox(
                    text = "",
                    padding = 1,
                ),
                widget.Clock(
                    format='%I:%M %p',
                    foreground = colors[1]),
            ],
            24,
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentScreen(padding=10,
                                     active_text = "⬤",
                                     inactive_text = "⬤",
                                     active_color = colors[1],
                                     inactive_color = colors[0]),
                # to indicate screen number
                widget.TextBox(
                    text = " 4 ",
                    foreground = colors[0]),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.GroupBox(hide_unused = True),
                widget.TextBox(
                    text = "//",
                    padding = 1,
                    foreground = colors[1]
                ),
                widget.WindowName(foreground = colors[1]),
                widget.TextBox(
                    text = "Volume: ",
                    padding = 1,
                ),
                widget.Volume(foreground = colors[1]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.Clock(
                    format='%m-%d-%Y %a',
                    foreground = colors[0]),
                widget.TextBox(
                    text = " // ",
                    padding = 1,
                ),
                widget.TextBox(
                    text = "",
                    padding = 1,
                ),
                widget.Clock(
                    format='%I:%M %p',
                    foreground = colors[1]),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
